set -x MANPAGER "nvim -c 'set ft=man' -"
set -x theme_color_scheme gruvbox
set -g theme_date_format "+%H:%M"
set -g theme_newline_cursor yes
status --is-interactive; and source (pyenv init -|psub)
status --is-interactive; and source (pyenv virtualenv-init -|psub)
set -g theme_display_virtualenv yes
set -g theme_nerd_fonts yes
set -x LS_COLORS (ls_colors_generator)
set -x PATH /home/patrick/git/work/chromedriver/ $PATH
