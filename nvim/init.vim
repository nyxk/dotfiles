syntax on
set rtp+=~/.fzf
call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'w0rp/ale'
Plug 'vim-airline/vim-airline-themes'
Plug 'mhinz/vim-signify'
Plug 'zchee/deoplete-jedi'
Plug 'Shougo/deoplete.nvim'
Plug 'davidhalter/jedi-vim'
Plug 'davidhalter/jedi'
Plug 'justinmk/vim-dirvish'
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'easymotion/vim-easymotion'
Plug 'sbdchd/neoformat'
Plug 'lervag/vimtex'
Plug 'tpope/vim-unimpaired'
call plug#end()
set nu
set cindent
set tabstop=4
set expandtab
set shiftwidth=4
set backspace=indent,eol,start
set autoindent
set ignorecase
set smartcase
set cursorline
set cursorcolumn
" gruvbox
set background=dark    " Setting dark mode
colorscheme gruvbox
let g:gruvbox_contrast_dark = "hard"
set termguicolors

" airline
let g:airline_powerline_fonts = 1

" ale
let g:ale_lint_on_text_changed = 'always'
let g:ale_linters = {
\   'python': ['flake8'],
\}
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" git signify
let g:signify_vcs_list = [ 'git']
nn ]h :SignifyToggleHighlight<CR>

" jedi
let g:deoplete#enable_at_startup = 1
let g:jedi#completions_enabled = 0
"let g:python3_host_prog = '/home/patrick/.pyenv/versions/defaultjedi/bin/python'
" work:
"let g:python3_host_prog = '/home/patrick/.pyenv/versions/3.6.1/envs/defaultjedi/bin/python'

" fzf
nn <C-p> :Files<CR>
nn <M-p> :Ag<CR>

" Ag with file preview toggled by '?'
command! -bang -nargs=* Ag
  \ call fzf#vim#ag(<q-args>,
  \                 <bang>0 ? fzf#vim#with_preview('up:60%')
  \                         : fzf#vim#with_preview('right:50%:hidden', '?'),
  \                 <bang>0)

" ex mode bye
nn Q :qa<CR>

" highlight
nn <C-h> :noh<CR>
" strip ws
nnoremap <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

" easymotion
map <Leader>l <Plug>(easymotion-lineforward)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <Leader>h <Plug>(easymotion-linebackward)
nmap <Leader>w <Plug>(easymotion-overwin-w)
let g:EasyMotion_startofline = 0 " keep cursor column when JK motion

" window focus
nmap <silent> <m-l> :wincmd l<CR>
nmap <silent> <m-h> :wincmd h<CR>
nmap <silent> <m-k> :wincmd k<CR>
nmap <silent> <m-j> :wincmd j<CR>

" autopep8
let g:neoformat_python_autopep8 = {
            \ 'exe': 'autopep8',
            \ 'args': ['--max-line-length 120']
            \ }

let g:neoformat_enabled_python = ['autopep8']
map gj :Neoformat<CR>

" copen
nmap <C-l> :Glog<CR> :copen<CR>

" pundo
set undofile
set undodir=~/.config/nvim/undo

" Copy to clipboard
vnoremap  <leader>y  "+y
nnoremap  <leader>Y  "+yg_
nnoremap  <leader>y  "+y
nnoremap  <leader>yy  "+yy

" " Paste from clipboard
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

" treat .sh as bash all the time
autocmd BufNewFile,BufRead *.sh 
        \ let b:is_bash=1 |
        \ set ft=sh
