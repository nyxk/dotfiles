function re --description 'Edit a remote file'
    echo (get --prompt="sudo pw: " --silent) > .open.remote.password
    echo $argv[1] > .open.remote.file
    nvim scp://$argv[1]/$argv[2] 
    rm .open.remote.file
end
