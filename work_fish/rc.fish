function rc --description "run a remote command"
    if test -f .open.remote.file
        set password (cat .open.remote.password)
        ssh -t (cat .open.remote.file) "echo $password | sudo -S $argv[1..-1]"
    else 
        if test (count $argv) -gt 1
            ssh -t $argv[1] sudo $argv[2..-1]
        else
            echo "Gonna need more arguments"
        end
    end
end
