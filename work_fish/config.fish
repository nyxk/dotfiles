set -xg MANPAGER "nvim -c 'set ft=man' -"
set -xg theme_color_scheme gruvbox
set -g theme_date_format "+%H:%M"
set -g theme_newline_cursor yes
export PATH="/home/patrick/chromedriver:$PATH"
set -x COLORTERM "gnome-terminal"
set -x PYENV_ROOT "$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
status --is-interactive; and source (pyenv init -|psub)
status --is-interactive; and source (pyenv virtualenv-init -|psub)
set -x LS_COLORS (ls_colors_generator)
