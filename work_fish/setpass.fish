function setpass
    set -U tmpVar $argv[1]
	get --prompt=">" --silent | read -U $argv[1]
    echo "Done"
    if set -q argv[2]
        set -U sleepTime $argv[2]
    else
        set -U sleepTime 10
    end
    fish -c 'sleep $sleepTime; set -e $tmpVar; echo Cleared' &
end
