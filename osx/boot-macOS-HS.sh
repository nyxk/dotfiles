#!/bin/bash

# See https://www.mail-archive.com/qemu-devel@nongnu.org/msg471657.html thread.
#
# The "pc-q35-2.4" machine type was changed to "pc-q35-2.9" on 06-August-2017.
#
# The "media=cdrom" part is needed to make Clover recognize the bootable ISO
# image.

##################################################################################
# NOTE: Comment out the "MY_OPTIONS" line in case you are having booting problems!
##################################################################################

MY_OPTIONS="+aes,+xsave,+avx,+xsaveopt,avx2,+smep"
export PULSE_SERVER unix:/tmp/pulse
ip tuntap add dev tap0 mode tap
ip link set tap0 up promisc on
brctl addif virbr0 tap0 

#sh ./bind.sh 0000:03:00.0

/opt/qemu-test/bin/qemu-system-x86_64 -enable-kvm -m 8192 -cpu Penryn,kvm=on,vendor=GenuineIntel,+invtsc,vmware-cpuid-freq=on,$MY_OPTIONS\
	  -machine pc-q35-2.9 \
	  -smp 4,cores=2 \
	  -usb -device usb-kbd -device usb-tablet \
	  -device vfio-pci,host=01:00.0,bus=pcie.0,multifunction=on \
	  -device vfio-pci,host=01:00.1,bus=pcie.0 \
	  -device isa-applesmc,osk="ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc" \
	  -drive if=pflash,format=raw,readonly,file=OVMF_CODE-pure-efi.fd \
	  -drive if=pflash,format=raw,file=OVMF_VARS-pure-efi-1024x768.fd \
	  -smbios type=2 \
	  -device ide-drive,bus=ide.2,drive=Clover \
	  -drive id=Clover,if=none,snapshot=on,format=qcow2,file=./'Clover.qcow2' \
	  -device ide-drive,bus=ide.1,drive=MacHDD \
	  -drive id=MacHDD,if=none,file=/mnt/Yin/osx_storage/mac_hdd.img,format=qcow2 \
	  -monitor stdio \
      -netdev tap,ifname=tap0,script=no,downscript=no,id=hostnet0 \
	  -device ich9-intel-hda -device hda-duplex \
      -device e1000-82545em,netdev=hostnet0,id=net0,mac=52:54:00:08:82:d6 \
      -object input-linux,id=mouse1,evdev=/dev/input/event4 -object input-linux,id=kbd2,evdev=/dev/input/event7,grab_all=on,repeat=on,rhotkey=56
	  #-device ide-drive,bus=ide.0,drive=MacDVD \
	  #-drive id=MacDVD,if=none,snapshot=on,media=cdrom,file=./'macOS_High_Sierra_10_13_1_Official.iso' \
	  #-device vfio-pci,host=01:00.0,bus=pcie.0,multifunction=on \
	  #-device vfio-pci,host=01:00.1,bus=pcie.0 \
	  #-netdev tap,id=net0,ifname=tap0,script=no,downscript=no -device e1000-82545em,netdev=net0,id=net0,mac=52:54:00:c9:18:27 \
      #-device intel-hda,id=sound0,bus=pci.0,addr=0x4 -device hda-duplex,id=sound0-codec0,bus=sound0.0,cad=0 \
      #-device vfio-pci,host=03:00.0,bus=pcie.0 \
