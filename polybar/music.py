#!/usr/bin/python3

import i3ipc

i3 = i3ipc.Connection()
tree = i3.get_tree()
for i in tree.leaves():
    if "Google Play Music Desktop Player" in i.window_class:
        if i.name is "Google Play Music Desktop Player":
            print("")
        else:
            print(i.name)
